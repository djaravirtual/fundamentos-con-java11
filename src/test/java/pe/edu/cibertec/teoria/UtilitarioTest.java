package pe.edu.cibertec.teoria;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import pe.edu.cibertec.capitulo11.Utilitario;

public class UtilitarioTest {

    @BeforeAll
    public static void beforeEachTest(){
        System.out.println("Acciones a ejecutar antes de ejecutar los test");
    }
    @AfterAll
    public static void afterEachTest(){
        System.out.println("Acciones a ejecutar despues de los test");
    }

    @Test
    public void testCalcularPromedio(){
        System.out.println("Test Calcular Promedio");
        Utilitario util = new Utilitario();
        double promedio = util.calcularPromedio(14,18);
        assertEquals(16,promedio);
    }

    @Test
    public void testEsNumeroImpar(){
        System.out.println("Test Es Numero Impar");
        Utilitario util = new Utilitario();
        Boolean esImpar = util.comprobarImpar(5);
        assertTrue(esImpar);
    }

    @Test
    public void testNoEsNumeroImpar(){
        System.out.println("Test No Es Numero Impar");
        Utilitario util = new Utilitario();
        Boolean noEsImpar = util.comprobarImpar(10);
        assertFalse(noEsImpar);
    }

}
