package pe.edu.cibertec.laboratorio;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import pe.edu.cibertec.capitulo11.Empleado;

public class EmpleadoTest {

    public EmpleadoTest(){}

    @Test
    public void testCalculaNetoCargoGerente(){
        System.out.println("Calcular Neto - Gerente");
        Empleado empleado = new Empleado();
        empleado.setCargo("Gerente");
        empleado.setSueldo(1000);
        double expResult = 950.;
        assertEquals(expResult, empleado.calcularNeto());
    }

    @Test
    public void testCalculaNetoCargoAsistente(){
        System.out.println("Calcular Neto - Asistente");
        Empleado empleado = new Empleado();
        empleado.setCargo("Asistente");
        empleado.setSueldo(1000);
        double expResult = 850.0;
        assertEquals(expResult, empleado.calcularNeto());

    }

    @Test
    public void testCalculaNetoCargoAdministrativo(){
        System.out.println("Calcular Neto - Administrativo");
        Empleado empleado = new Empleado();
        empleado.setCargo("Administrativo");
        empleado.setSueldo(1000);
        double expResult = 500.0;
        assertEquals(expResult, empleado.calcularNeto());
    }

    @Test
    public void testCalculaNetoCargoTecnico(){
        System.out.println("Calcular Neto - Tecnico");
        Empleado empleado = new Empleado();
        empleado.setCargo("Tecnico");
        empleado.setSueldo(1000);
        double expResult = 200.;
        assertEquals(expResult, empleado.calcularNeto());
    }

}
