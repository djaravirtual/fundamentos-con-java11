package pe.edu.cibertec;

import pe.edu.cibertec.capitulo2.CalculadoraWrapper;
import pe.edu.cibertec.capitulo2.TeoriaCapitulo2;
import pe.edu.cibertec.capitulo3.TeoriaCapitulo3;
import pe.edu.cibertec.capitulo5.TeoriaCapitulo5;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        System.out.println("Hello world!");
        System.out.println("Learning Java11 fundamentals!");

        //TeoriaCapitulo2.showSession01();
        //TeoriaCapitulo2.showSesion01PracticaLaboratorio02();
        //TeoriaCapitulo2.showSesion01TrabajoDeCaja();

        //TeoriaCapitulo3.showSession01();
        //TeoriaCapitulo3.showLaboratorio01();
        //TeoriaCapitulo3.showSession02();

        TeoriaCapitulo5.showLaboratorio02();

    }

}