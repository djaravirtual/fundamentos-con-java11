package pe.edu.cibertec.homework.trabajo02;

@FunctionalInterface
public interface OperationMath {
    double calcular(int number);
}
