package pe.edu.cibertec.homework.trabajo02;

@FunctionalInterface
public interface OperationArray {
    int calcular(int[] number);
}
